<?php

namespace App\Controller;


use App\Repository\CustomArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Jdesca\Infra\Entity\Article;
use Jdesca\Infra\Form\ArticleType;
use Jdesca\Infra\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("articles")
 */
class ArticleController extends AbstractFOSRestController
{
    /**
     * @var CustomArticleRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(CustomArticleRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Rest\Get()
     */
    public function all(){
        return $this->handleView($this->view($this->repository->findByTitle("lorem")));
    }

    /**
     * @Rest\Get("/{id}")
     */
    public function getArticle(int $id){
        return $this->handleView($this->view($this->repository->findIdTwo()));
    }

    /**
     * @Rest\Post()
     */
    public function post(Request $request){
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request->request->all());
        if($form->isValid()){
            $this->entityManager->persist($article);
            $this->entityManager->flush();
            return $this->handleView($this->view($article));
        }
        return $this->handleView($this->view($form));
    }

    /**
     * @Rest\Put("/{id}")
     */
    public function put(Request $request, int $id){
        $article = $this->repository->find($id);
        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request->request->all());
        if($form->isValid()){
            $this->entityManager->persist($article);
            $this->entityManager->flush();
            return $this->handleView($this->view($article));
        }

        $errorString = [];
        foreach ($form->getErrors(true) as $error){
            $errorString[$error->getOrigin()->getName()] = $error->getMessage();
        }
        return $this->handleView($this->view($form));
    }

    /**
     * @Rest\Delete("/{id}")
     */
    public function delete(Request $request, int $id){
        $article = $this->repository->find($id);
        $this->entityManager->remove($article);
        return $this->json("OK");
    }
}