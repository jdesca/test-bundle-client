<?php


namespace App\Repository;


use Jdesca\Infra\Repository\ArticleRepository;

class CustomArticleRepository extends ArticleRepository
{
    /**
     * recherche d'article par titre
     */
    public function findByTitle(string $title){
        return $this->createQueryBuilder('article')
            ->where("article.title = (:title)")->setParameter("title", "lorem")
            ->getQuery()->execute();
    }

    /**
     * récup l'article d'id 2
     */
    public function findIdTwo(){
        return $this->find(2);
    }
}